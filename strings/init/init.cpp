#include <QTextStream>
#include<string>

int main(void) {

	QTextStream out(stdout);
	// Inicialización básica
	QString str1 = "Hola caracola";
	out << str1 << endl;
	// Inicialización en forma de objeto
	QString str2("Mi gozo en un pozo");
	out << str2 << endl;
	// Inicialización trasformando una string a c_string
	std::string s1 = "Nunca digas nunca";
	QString str3 = s1.c_str(); 
	out << str3 << endl;
	// Inicialización con una string (hay que pasar el contenido y el tamaño)
	std::string s2 = "Te pincho la cabeza";
	QString str4 = QString::fromLatin1(s2.data(), s2.size());
	out << str4 << endl;
	// Inicialización a partir de una c_string
	char s3[] = "Hasta luego Maricarmen";
	QString str5(s3);
	out << str5 << endl;

	return 0;
}
