#include<QTextStream>

int main() {
	QTextStream out(stdout);
	// Inicialización
	QString a = "love";
	// Añadir al final
	a.append(" you");
	// Añadir al principio
	a.prepend("I ");

	out<<a<<endl;
	out<<"String has: "<<a.count()<<" characters\n";
	out<<a.toUpper()<<endl; // No modifica el string, devuelve una copia
	out<<a.toLower()<<endl; // Igual que toUpper()

	return 0;
}

